# Verwende das offizielle Nginx-Image als Basis
FROM nginx:1.23-alpine

# Kopiere die lokale HTML-Datei in das Nginx-Verzeichnis
COPY index.html /usr/share/nginx/html

# Optional: Falls Anpassungen an der nginx-Konfiguration erforderlich sind, können sie hier vorgenommen werden
# COPY nginx.conf /etc/nginx/nginx.conf

# Der Port, auf dem der Nginx-Server laufen soll (standardmäßig 80)
EXPOSE 80